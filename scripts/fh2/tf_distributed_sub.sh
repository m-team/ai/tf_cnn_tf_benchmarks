#!/bin/bash

TIMESTAMP=$(date +%y%m%d_%H%M%S)

export UCONTAINER="tfbench110-gpu"
export HOST_RUN_RESULTS=$HOME/workspace/tf_cnn_benchmarks-tests

### Distributed Training Settings
export NumParamServers=2
export NumWorkers=2
export PARAM_SERVER_PORT=2222
export WORKER_PORT=2223

### Model Training Settings
HOST_DATA=$HOME/datasets/imagenet
export DATASET=synthetic        # imagenet
export NUM_BATCHES=50
export NUM_EPOCHS=1
export MODEL=resnet50
export BATCH_SIZE=64  #64
export NUM_GPUS=2

# Where to store output
export HOST_RUN=$HOST_RUN_RESULTS/${TIMESTAMP}_udocker_${DATASET}_${NumParamServers}ps_${NumWorkers}w
# Check if local directories exist
if [ ! -d "$HOST_RUN" ]; then
   mkdir -p $HOST_RUN
fi

for (( i=0; i<${NumParamServers}; i++ ));
do
    export TFServerID=$i
    export TF_SERVER_TYPE=ps
    SLURM_PARAMS="-p visu --job-name='tf_ps${TFServerID}' \
--nodes=1 --ntasks-per-node=24 --time=12:00:00 \
--error $HOST_RUN/job_param_server.err \
--output $HOST_RUN/job_param_server.out"
    export ParamServerID=$i
    export WorkerID=$i
    sbatch $SLURM_PARAMS ./job_distributed_ps+w.sh
    #sbatch $SLURM_PARAMS ./job_distributed.sh 
    sleep 1
done

#for (( j=0; j<${NumWorkers}; j++ ));
#do
#    export TFServerID=$j
#    export TF_SERVER_TYPE=worker
#    SLURM_PARAMS="-p visu --job-name='tf_worker${TFServerID}' \
#--nodes=1 --ntasks-per-node=24 --time=12:00:00 \
#--error $HOST_RUN/job_worker.err \
#--output $HOST_RUN/job_worker.out"
#    sbatch $SLURM_PARAMS ./job_distributed.sh 
#    sleep 1
#done
#wait

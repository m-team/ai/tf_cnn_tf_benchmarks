#!/bin/bash
#####################################
# Script to submit N jobs 
# via slurm command on a GPU cluster
#####################################
DATENOW=$(date +%y%m%d_%H%M%S)

CIFAR10_DIR="$HOME/datasets/cifar10/data"
IMAGENET_DIR="$HOME/datasets/imagenet/data"

export HOST_RUN_RESULTS="$HOME/workspace/tf_cnn_benchmarks-tests"
export DATASET_DIR=$IMAGENET_DIR

##### JOB CONFIG #####
NUM_NODES=4
NTASKS=4

##### DEFAULT CONFIG #####
export CONTAINER="tfbench120-horovod"	# Container to run
export NUM_GPUS="1"			# Want (1,2,4). In case of Horovod use 1 (!)
export DATASET="imagenet"		# Dataset to use for training (synthetic, cifar10, imagenet) # imagenet
export MODEL="resnet50"			# Used CNN model (resnet50(for synthetic data), resnet56, alexnet)
export NUM_EPOCHS=100			# Number of epochs for training with REAL data
export NUM_BATCHES=100          	# Number of epochs for synthetic data
export BATCH_SIZE=64			# Batch size per GPU, e.g. ResNet, InceptionV3 = 64, AlexNet = 512
#export NUM_PROCESS=2			# MPI Number of Processes

##### Create local directories to mount #####
DIR_EXT="${DATASET}_${MODEL}_${NUM_NODES}nodes_${NTASKS}tasks" # Directory format for output files
export HOST_RUN="${HOST_RUN_RESULTS}/${DATENOW}_udocker_${DIR_EXT}"
#export HOST_RUN="${HOST_RUN_RESULTS}/190513_231507_udocker_imagenet_resnet50_4gpus.6nodes"
export HOST_LOG="${HOST_RUN}/log"
export HOST_OUTPUT="${HOST_RUN}/output"

# check if local directories exist
if [ ! -d "$HOST_RUN" ]; then
   mkdir -p $HOST_RUN
   mkdir $HOST_LOG
   mkdir $HOST_OUTPUT
fi
# ------------------------

#JOB2RUN="./udocker_1job.sh"
JOB2RUN="./udocker_horovod.sh"
#JOB2RUN="./udocker_eval.sh"
JOBOUT=${HOST_RUN}/job_$DATENOW"_$DATASET"
NJOB=1
for j in $(seq 1 $NJOB)
do 
    echo "Submitting job $j out of $NJOB.."
    module load mpi/openmpi/4.0
    SLURM_PARAMS="-p visu --job-name=tfbench --nodes=${NUM_NODES} --ntasks-per-node=${NTASKS} --time=24:00:00 --output=${JOBOUT}.out --error=${JOBOUT}.err"
    echo "sbatch $SLURM_PARAMS $JOB2RUN"
    sbatch $SLURM_PARAMS $JOB2RUN
    #$JOB2RUN
    sleep 1s
done

#!/bin/bash
##########
# Script that runs the tf_cnn_benchmark via a udocker container.
# Passed arguments are parameters for the HPC-job.
# Runs one benchmark run as well as one evaluation run and logs the output.
##########

# ------------------------
UDOCKER_DIR="$PROJECT/.udocker" # Location of udocker and containers
UDOCKER_DATA="/benchmarks/data"
UDOCKER_RUN="/benchmarks/run_output"
UENV="CUDA_VISIBLE_DEVICES=0,1,2,3"
# ------------------------

##### USAGEMESSAGE #####
USAGEMESSAGE="Usage: sh $0 <options> ; where <options> are: \n
		--num_gpus={1|2|4}	    \t \t \t The amount of GPUs to run on \n
		--dataset={synthetic|cifar10}    \t The Dataset to use \n 
		--data_dir \t Directory for data on host \n 
		--model={resnet50|resnet56|alexnet} \t The CNN model to use \n
		--num_epochs \t number of epochs for training with REAL data (either this or --num_batches, NOT both!) \n 
		--num_batches \t number of batches for training with SYNTHETIC data" 
INFOMESSAGE="=> Should now process scripts"    # ???


##### PARSE SCRIPT FLAGS #####
arr=("$@")
if [ $# -eq 0 ]; then 
# use default config (0)
    echo "Using default config"
elif [ $1 == "-h" ] || [ $1 == "--help" ]; then 
# print usagemessage
    shopt -s xpg_echo
    echo $USAGEMESSAGE
    exit 1
elif [ $# -ge 1 ] && [ $# -le 6 ]; then 
# read benchmark options as parameters (1-3)
    for i in "${arr[@]}"; do
	echo "$i"
        [[ $i = *"--num_gpus"* ]]  && NUM_GPUS=${i#*=} 
        [[ $i = *"--dataset"* ]]  && DATASET=${i#*=} 
        [[ $i = *"--data_dir"* ]]  && DATASET_DIR=${i#*=}
        [[ $i = *"--model"* ]]  && MODEL=${i#*=}
        [[ $i = *"--num_epochs"* ]]  && NUM_EPOCHS=${i#*=}
        [[ $i = *"--num_batches"* ]]  && NUM_BATCHES=${i#*=}
        [[ $i = *"--batch_size"* ]]  && BATCH_SIZE=${i#*=}
    done
else
    # Too many arguments were given (>3)
    echo "ERROR! Too many arguments provided!"
    shopt -s xpg_echo    
    echo $USAGEMESSAGE
    exit 2
fi

echo $SLURM_JOB_NODELIST >> $HOST_RUN/horovod.list

HOST_DATA="$DATASET_DIR"
UDOCKER_LOG="${UDOCKER_RUN}/log"
UDOCKER_OUTPUT="${UDOCKER_RUN}/output"

### !!! comment this out
#if [ "$DATASET" != "synthetic" ]; then
#    HOST_DATA="${HOST_DATA}/${DATASET}"
#fi
###

##### OPTIONS FOR BENCHMARK AND EVAL RUN #####
#--optimizer=sgd
#--nodistortions \
#--weight_decay=1e-4 \
#--use_fp16 \
OPTIONS="--batch_size=${BATCH_SIZE} \
--optimizer=momentum \
--variable_update=horovod \
--nodistortions \
--weight_decay=1e-4 \
--use_fp16 \
--num_gpus=$NUM_GPUS \
--train_dir=${UDOCKER_OUTPUT} \
--log_dir=${UDOCKER_LOG} \
--benchmark_log_dir=${UDOCKER_LOG} \
--benchmark_test_id=$(date +%y%m%d_%H%M) \
--model=$MODEL"		# Has to be resnet50 if synthetic is selected

EVAL_OPTIONS=" --eval $OPTIONS"

if [ "$DATASET" != "synthetic" ]; then
# If Data is not synthetic, use data at specified file location
    OPTIONS="${OPTIONS} --data_dir=$UDOCKER_DATA --data_name=$DATASET"
    EVAL_OPTIONS=" --eval $OPTIONS" 
# If Data is not synthetic, use number of epochs
    OPTIONS="${OPTIONS} --num_epochs=${NUM_EPOCHS}"
fi

if [ "$DATASET" == "synthetic" ]; then
# If Data is synthetic, use number of batches
    OPTIONS="${OPTIONS} --num_batches=${NUM_BATCHES}"
fi

EVAL_OPTIONS="${EVAL_OPTIONS} --num_batches=200" #${NUM_BATCHES}"

#--eval_dir=$PWD/eval/ \ only for TFevent file


##### RUN THE JOB #####
echo "==================================="
echo "=> udocker container: $CONTAINER"
echo "=> Running on: $HOSTNAME"
echo "==================================="

echo "Enabling NVIDIA driver.."
#nvidia-smi
nvidia-modprobe -u -c=0

echo "Setting up Nvidia compatibility."
udocker setup --nvidia --force $CONTAINER   # Setup nvidia usage

echo "Running benchmark..."
export -n CUDA_VISIBLE_DEVICES

SCRIPT="python /benchmarks/tf_cnn_benchmarks.py $OPTIONS"  # benchmark script to run

MPI_PARAMS="-bind-to none -map-by slot \
-x NCCL_DEBUG=INFO -x LD_LIBRARY_PATH -x PATH \
-x HOROVOD_MPI_THREADS_DISABLE=1 \
-mca pmix_server_usock_connections 1 \
-mca pml ob1 -mca btl ^openib"

echo "mpirun $MPI_PARAMS \
udocker run --hostenv --hostauth --user=$USER -v ${HOST_DATA}:${UDOCKER_DATA} -v ${HOST_RUN}:${UDOCKER_RUN} ${CONTAINER} $SCRIPT"

mpirun $MPI_PARAMS \
udocker run --hostenv --hostauth --user=$USER -v ${HOST_DATA}:${UDOCKER_DATA} -v ${HOST_RUN}:${UDOCKER_RUN} ${CONTAINER} $SCRIPT

mv $HOST_LOG/benchmark_run.log  $HOST_LOG/benchmark.log # Rename run_benchmark.log since it is overwritten in eval run 

echo "Done with benchmark. Running evaluation..."

SCRIPT="python /benchmarks/tf_cnn_benchmarks.py $EVAL_OPTIONS"  # evaluation script to run
##udocker run -v ${HOST_DATA}:${UDOCKER_DATA} -v ${HOST_RUN}:${UDOCKER_RUN} ${CONTAINER} $SCRIPT
udocker run -v ${HOST_DATA}:${UDOCKER_DATA} -v ${HOST_RUN}:${UDOCKER_RUN} -e ${UENV} ${CONTAINER} $SCRIPT
mv $HOST_LOG/benchmark_run.log  $HOST_LOG/eval.log 

### Cleanup
#rm ${HOST_OUTPUT}/*

echo "Done. Logfiles written to: ${HOST_LOG}."

#!/bin/bash
#####################################
# Script to submit N jobs 
# via bsub command on Juron GPU cluster
#####################################
DATENOW=$(date +%y%m%d_%H%M%S)
RUN_MODE=bmetal   # b(are)metal, udocker, singularity

CIFAR10_DIR="$HOME/datasets/cifar10/data"
IMAGENET_DIR="$HOME/datasets/imagenet/data"

export HOST_RUN_RESULTS="$HOME/benchmarks.1"
export DATASET_DIR=$IMAGENET_DIR

##### DEFAULT CONFIG #####
export CONTAINER="tfbench110-gpu"	# Container to run
export NUM_GPUS="4"			# Want (1,2,4)
export DATASET="synthetic"		# Dataset to use for training (synthetic, cifar10) # imagenet
export MODEL="resnet50"			# Used CNN model (resnet50(for synthetic data), resnet56, alexnet)
export NUM_EPOCHS=5			# Number of epochs for training with REAL data
export NUM_BATCHES=500          	# Number of epochs for synthetic data
export BATCH_SIZE=64			# Batch size per GPU, e.g. ResNet, InceptionV3 = 64, AlexNet = 512
export NUM_PROCESS=2			# MPI Number of Processes

##### Create local directories to mount #####
DIR_EXT="${DATASET}_${MODEL}_${NUM_GPUS}_gpu" # Directory format for output files
export HOST_RUN="${HOST_RUN_RESULTS}/${DATENOW}_${RUN_MODE}_${DIR_EXT}"

# check if local directories exist
if [ ! -d "$HOST_RUN" ]; then
   mkdir -p $HOST_RUN
fi
# ------------------------

#JOB2RUN="./udocker_1job.sh"
JOB2RUN="./job_baremetal.sh"
JOBOUT=${HOST_RUN}/job_$DATENOW"_$DATASET"
NJOB=1

for j in $(seq 1 $NJOB)
do 
    echo "Submitting job $j out of $NJOB.."
    # -n ${NUM_HOSTS}
    #bsub -R "${NUM_HOSTS}*{span[ptile=1] rusage[ngpus_physical=${NUM_GPUS}]}" -e $HOST_RUN/job_$DATENOW.err -o $HOST_RUN/job_$DATENOW.out $JOB2RUN
    bsub -gpu "num=${NUM_GPUS}" -e $HOST_RUN/job_$DATENOW.err -o $HOST_RUN/job_$DATENOW.out $JOB2RUN
    sleep 1s
done

#!/bin/bash

#TIMESTAMP=$(date +%s)
TIMESTAMP=$(date +%y%m%d_%H%M%S)
ThisHOST=$(hostname -s)

if [ "$TF_SERVER_TYPE" == "ps" ]; then
    echo $ThisHOST:$PARAM_SERVER_PORT >> $TFBENCH_OUTPUT/param_servers.list
    echo "[INFO] param_serverID=$TFServerID from $NumParamServers"
fi

if [ "$TF_SERVER_TYPE" == "worker" ]; then
    echo $ThisHOST:$WORKER_PORT >> $TFBENCH_OUTPUT/workers.list
    echo "[INFO] workerID=$TFServerID from $NumWorkers"
fi

TRAIN_DIR=$TFBENCH_RUN/${TF_SERVER_TYPE}${TFServerID}-output
LOG_DIR=$TFBENCH_RUN/${TF_SERVER_TYPE}${TFServerID}-log

# Check if all param server jobs has started
n_param_servers_on=0
while [ $n_param_servers_on -lt $NumParamServers ]
do
    sleep 5
    PARAM_SERVERS=($(cat $TFBENCH_OUTPUT/param_servers.list))
    n_param_servers_on=${#PARAM_SERVERS[@]}
done

# Check if all worker jobs has started
n_workers_on=0
while [ $n_workers_on -lt $NumWorkers ]
do
    sleep 5
    WORKERS=($(cat $TFBENCH_OUTPUT/workers.list))
    n_workers_on=${#WORKERS[@]}
done

# https://stackoverflow.com/questions/1527049/how-can-i-join-elements-of-an-array-in-bash
PARAM_SERVERS_LIST=$(IFS=, ; echo "${PARAM_SERVERS[*]}")
#echo $PARAM_SERVERS_LIST

WORKERS_LIST=$(IFS=, ; echo "${WORKERS[*]}")
#echo $WORKERS_LIST

OPTIONS="--batch_size=$BATCH_SIZE \
--optimizer=sgd \
--variable_update=distributed_replicated \
--cross_replica_sync=True \
--local_parameter_device=CPU \
--train_dir=$TRAIN_DIR \
--log_dir=$LOG_DIR \
--benchmark_log_dir=$LOG_DIR \
--num_gpus=$NumGPUs \
--model=$MODEL \
$DATASET_OPTIONS \
--ps_hosts=$PARAM_SERVERS_LIST \
--worker_hosts=$WORKERS_LIST \
--benchmark_test_id=${TF_SERVER_TYPE}_${TIMESTAMP} \
--job_name=$TF_SERVER_TYPE \
--task_index=$TFServerID"

echo "python tf_cnn_benchmarks.py $OPTIONS"
python tf_cnn_benchmarks.py $OPTIONS

## Script to create an new docker image for the benchmarking
## IMPORTANT: Only run form main directory in order to get a working image! 

wd="$(dirname "$0")"
echo $wd

# Build new image for gpu benchmark
echo "Building new Image agrupp/tf_cnn_benchmark_gpu"
docker build -t agrupp/tf_cnn_benchmark_gpu -f ${wd}/../docker/Dockerfile.gpu .
echo "\n"

# Push it to dockerhub
echo 'Pushing agrupp/tf_cnn_benchmark_gpu to Dockerhub' 
docker image push agrupp/tf_cnn_benchmark_gpu:latest
echo "\n"

# Remove previous, now untagged image
echo 'Removing dangling & untagged images'
docker images -q --filter dangling=true | xargs docker rmi
echo "\n"

# Show the available images
docker images

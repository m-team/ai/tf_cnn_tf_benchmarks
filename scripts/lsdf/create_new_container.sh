### Script to pull and build a new container via udocker ###

# Pull newest image from Dockerhub
echo "Pulling newest image from agrupp/tf_cnn_benchmark_gpu:"
udocker pull agrupp/tf_cnn_benchmark_gpu
printf '\n'

# Try to delete old image, if still existant
udocker rm tf-gpu
printf '\n'

# Create new container with proper name
echo "Creating new container 'tf-gpu'..."
udocker create --name=tf-gpu agrupp/tf_cnn_benchmark_gpu
printf '\n'

# Activate nvidia compability for gpu usage
echo "Setup nvidia usage:"
udocker setup --nvidia tf-gpu


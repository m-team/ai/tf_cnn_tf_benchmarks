##############################################################################
# Script that runs the tf_cnn_benchmark via a Singularity image several times
# for all needed parameter combinations.
# Runs one benchmark run as well as one evaluation run and logs the output.
##############################################################################
#TODO Fix warnings at begin of runs

timestamp() {
date +"%Y-%m-%d_%H.%M"
}

##### DEFAULT CONFIG #####
CONTAINER="$PWD/tf_cnn_benchmark_gpu.simg" # Image to run
# ------------------------
HOST_DATA="$PWD/datasets/cifar10"  
HOST_LOG="$PWD/benchmarks_singularity/bench_$(timestamp)"
HOST_OUTPUT="$PWD/benchmarks_singularity/bench_$(timestamp)/output"
# ------------------------
SINGULARITY_LOG="/benchmarks/log"
SINGULARITY_DATA="/benchmarks/data"
SINGULARITY_OUTPUT="/benchmarks/output"
# ------------------------

### CURRENT PARAMETERS - #iterations, #gpus and datasets ###
NUM_ITERS=1
declare -a NUMGPUS=("1" "2" "4")
declare -a DATASET=("synthetic" "cifar10" "imagenet")


##### USAGEMESSAGE #####
#TODO
#TODO: pipe output to some logfiles
USAGEMESSAGE="Usage: sh $0 <options> ; where <options> are: \n"


##### OPTIONS FOR BENCHMARK AND EVAL RUN #####
OPTIONS="--batch_size=64 \
--optimizer=sgd \
--variable_update=parameter_server \
--local_parameter_device=cpu \
--train_dir=${SINGULARITY_OUTPUT} \
--log_dir=${SINGULARITY_LOG} \
--benchmark_log_dir=${SINGULARITY_LOG} \
--benchmark_test_id=$(date +%y%m%d_%H%M) \
--num_epochs=5"

EVAL_OPTIONS=" --eval \
--train_dir=${SINGULARITY_OUTPUT} \
--benchmark_log_dir=${SINGULARITY_LOG} \
--num_gpus=$NUMGPUS "
#--eval_dir=$PWD/eval/ \ only for TFevent file


## Run multiple benchmarks, one for each parameter permutation ##
## Iterate $NUM_ITERS times for averaging results ##
i=0
counter=1
until [ $counter -gt $NUM_ITERS ]
do
  for num in "${NUMGPUS[@]}"
  do
    run_opt="$OPTIONS --num_gpus=$num"
    eval_opt="$EVAL_OPTIONS --num_gpus=$num"

    # Take care that we do not occupy all gpus, since they are cross-interconnected
    if [ $num != 4 ]
    then
      export CUDA_VISIBLE_DEVICES=1,3
    else
      unset CUDA_VISIBLE_DEVICES
    fi

    for data in "${DATASET[@]}"
    do
      # Different models and file locations for diferent datasets
      if [ "$data" == "synthetic" ]
      then
        declare -a MODELNAME=("resnet50" "alexnet")
	run_opt_1=$run_opt
	eval_opt_1=$eval_opt

      elif [ "$data" == "imagenet" ]
      then
        declare -a MODELNAME=("resnet56" "alexnet")
	run_opt_1=$run_opt
        HOST_DATA="$PWD/../eo9869/datasets/imagenet/"
        run_opt_1="$run_opt --data_name=$data --data_dir=$SINGULARITY_DATA"
        eval_opt_1="$eval_opt --data_name=$data --data_dir=$SINGULARITY_DATA"

      elif [ "$data" == "cifar10" ]
      then
        declare -a MODELNAME=("resnet56" "alexnet")
        HOST_DATA="$PWD/datasets/cifar10"
        run_opt_1="$run_opt --data_name=$data --data_dir=$SINGULARITY_DATA"
        eval_opt_1="$eval_opt --data_name=$data --data_dir=$SINGULARITY_DATA"
      fi

      # Run each corresponding model for a dataset
      for model in "${MODELNAME[@]}"
      do 
        run_opt_2="$run_opt_1 --model=$model"
        eval_opt_2="$eval_opt_1 --model=$model"

        ## Create local directories to mount ##
        DIR_EXT="${data}_${model}_${num}_gpu" # Directory format for output files
        
        h_log="${HOST_LOG}/${DIR_EXT}/${counter}"
        h_out="${HOST_OUTPUT}/${DIR_EXT}/${counter}"
        mkdir -p $h_log
        mkdir -p $h_out
  
        ## Actually run the benchmark and evaluation ##
        echo "======================================"
        echo "=> Singularity container: $CONTAINER"
        echo "=> Current parameters: $num gpus, $data, $model"
        echo "=> Iteration: $counter"
        echo "======================================"
        
        #### BENCHMARK ####
        SCRIPT="python3 /benchmarks/tf_cnn_benchmarks.py $run_opt_2"  # benchmark script to run
        singularity exec --nv -B ${HOST_DATA}:${SINGULARITY_DATA} -B ${h_out}:${SINGULARITY_OUTPUT} -B ${h_log}:${SINGULARITY_LOG} $CONTAINER $SCRIPT
        mv $h_log/benchmark_run.log  $h_log/benchmark.log # Rename run_benchmark.log since it is overwritten in eval run 
        
        echo "Done with benchmark. Running evaluation..."
        
        #### EVALUATION ####
        SCRIPT="python3 /benchmarks/tf_cnn_benchmarks.py $eval_opt_2"  # evaluation script to run
        singularity exec --nv -B ${HOST_DATA}:${SINGULARITY_DATA} -B ${h_out}:${SINGULARITY_OUTPUT} -B ${h_log}:${SINGULARITY_LOG} $CONTAINER $SCRIPT
        mv $h_log/benchmark_run.log  $h_log/eval.log 
        
        echo "Finished Iteration. Logfiles written to: ${h_log}."
        
        rm ${h_out}/*
        let i+=1
      done
    done
  done
  let counter+=1
done

rm -r $HOST_OUTPUT # Not needed for our evaluation
echo "Done. Ran $i benchmarks."	

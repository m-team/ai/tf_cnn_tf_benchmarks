############################################################################
# Script that runs the tf_cnn_benchmark via a udocker container
# several times for all needed parameter combinations.
# Runs one benchmark run as well as one evaluation run and logs the output.
#
# NOTE: The udocker container needs to be set up with nvidia compatibility
############################################################################

timestamp() {
date +"%Y-%m-%d_%H.%M"
}

##### DEFAULT CONFIG #####
CONTAINER="tf-gpu" # Container to run
# ------------------------
HOST_LOG="$PWD/benchmarks_udocker/bench_$(timestamp)"
HOST_OUTPUT="$PWD/benchmarks_udocker/bench_$(timestamp)/output"
HOST_DATA="$PWD/datasets/cifar10"
# ------------------------
UDOCKER_DIR="$PWD/.udocker" # Location of udocker and containers
UDOCKER_LOG="/benchmarks/log"
UDOCKER_DATA="/benchmarks/data"
UDOCKER_OUTPUT="/benchmarks/output"
# ------------------------


### CURRENT PARAMETERS - #iterations, #gpus and datasets ###
NUM_ITERS=1 
declare -a NUMGPUS=("1" "2" "4")
declare -a DATASET=("imagenet" "synthetic" "cifar10")


##### USAGEMESSAGE #####
#TODO
#TODO: pipe output to some logfiles
USAGEMESSAGE="Usage: sh $0 <options> ; where <options> are: \n"


##### OPTIONS FOR BENCHMARK AND EVAL RUN #####
OPTIONS="--batch_size=64 \
--optimizer=sgd \
--variable_update=parameter_server \
--local_parameter_device=cpu \
--train_dir=${UDOCKER_OUTPUT} \
--log_dir=${UDOCKER_LOG} \
--benchmark_log_dir=${UDOCKER_LOG} \
--benchmark_test_id=$(date +%y%m%d_%H%M) \
--num_epochs=5"

EVAL_OPTIONS=" --eval \
--train_dir=${UDOCKER_OUTPUT} \
--benchmark_log_dir=${UDOCKER_LOG} \
--num_gpus=$NUMGPUS "
#--eval_dir=$PWD/eval/ \ only for TFevent file


## Run multiple jobs, one for each parameter permutation ##
## Iterate $NUM_ITERS times for averaging the results ##
i=0
counter=1
until [ $counter -gt $NUM_ITERS ]
do
  for num in "${NUMGPUS[@]}"
  do
    run_opt="$OPTIONS --num_gpus=$num"
    eval_opt="$EVAL_OPTIONS --num_gpus=$num"

    for data in "${DATASET[@]}"
    do
      # Different models and file locations for diferent datasets
      if [ "$data" == "synthetic" ]
      then
        declare -a MODELNAME=("resnet50" "alexnet")
	run_opt_1=$run_opt
	eval_opt_1=$eval_opt

      elif [ "$data" == "imagenet" ]
      then
        declare -a MODELNAME=("resnet50" "alexnet")
        HOST_DATA="$PWD/../eo9869/datasets/imagenet/"
        run_opt_1="$run_opt --data_name=$data --data_dir=$UDOCKER_DATA"
        eval_opt_1="$eval_opt --data_name=$data --data_dir=$UDOCKER_DATA"

      elif [ "$data" == "cifar10" ]
      then
        declare -a MODELNAME=("resnet56" "alexnet")
        HOST_DATA="$PWD/datasets/cifar10"
        run_opt_1="$run_opt --data_name=$data --data_dir=$UDOCKER_DATA"
        eval_opt_1="$eval_opt --data_name=$data --data_dir=$UDOCKER_DATA"
      fi

      # run each corresponding model for a dataset
      for model in "${MODELNAME[@]}"
      do
        run_opt_2="$run_opt_1 --model=$model"
        eval_opt_2="$eval_opt_1 --model=$model"

        ## Create local directories to mount ##
        DIR_EXT="${data}_${model}_${num}_gpu" # Directory format for output files
        
        h_log="${HOST_LOG}/${DIR_EXT}/${counter}"
        h_out="${HOST_OUTPUT}/${DIR_EXT}/${counter}"
        mkdir -p $h_log
        mkdir -p $h_out
        
        ## Actually run the benchmark and evaluation ##
        echo "========================================"
        echo "=> Udocker container: $CONTAINER"
        echo "=> Current parameters: $num gpus, $data, $model"
        echo "=> Iteration: $counter"
        echo "========================================"
          
	### BENCHMARK ###
        SCRIPT="python3 /benchmarks/tf_cnn_benchmarks.py $run_opt_2"  # benchmark script to run
        # Take care that we do not occupy all gpus, since they are cross-interconnected
        if [ $num != 4 ]
        then
          udocker run -e CUDA_VISIBLE_DEVICES=1,3 -v ${HOST_DATA}:${UDOCKER_DATA} -v ${h_out}:${UDOCKER_OUTPUT} -v ${h_log}:${UDOCKER_LOG} ${CONTAINER} $SCRIPT
        else 
          udocker run -v ${HOST_DATA}:${UDOCKER_DATA} -v ${h_out}:${UDOCKER_OUTPUT} -v ${h_log}:${UDOCKER_LOG} ${CONTAINER} $SCRIPT
        fi
        mv $h_log/benchmark_run.log  $h_log/benchmark.log # Rename run_benchmark.log since it is overwritten in eval run 
        echo "Done with benchmark. Running evaluation..."
          
        #### EVALUATION ####
        SCRIPT="python3 /benchmarks/tf_cnn_benchmarks.py $eval_opt_2"  # evaluation script to run
        # Take care that we do not occupy all gpus, since they are cross-interconnected
        if [ $num != 4 ]
        then
          udocker run -e CUDA_VISIBLE_DEVICES=1,3 -v ${HOST_DATA}:${UDOCKER_DATA} -v ${h_out}:${UDOCKER_OUTPUT} -v ${h_log}:${UDOCKER_LOG} ${CONTAINER} $SCRIPT
	else
          udocker run -v ${HOST_DATA}:${UDOCKER_DATA} -v ${h_out}:${UDOCKER_OUTPUT} -v ${h_log}:${UDOCKER_LOG} ${CONTAINER} $SCRIPT
	fi
        mv $h_log/benchmark_run.log  $h_log/eval.log 
        echo "Finished Iteration. Logfiles written to: ${h_log}. \n \n"
      
        rm ${h_out}/* # Cleanup, since out data is quite large!
        let i+=1
      done
    done
  done
  let counter+=1
done

rm -r $HOST_OUTPUT # Not needed for our evaluation
echo "Done. Ran $i benchmarks."
